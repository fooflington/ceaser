#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#define MOD(a,b) a >= 0 ? a % b : (a % b) + b

char do_shift(int distance, char c) {
    if( ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' ) ) {
        int offset = 0;
        if (c >= 'a' && c <= 'z')
            offset = 'a';

        if (c >= 'A' && c <= 'Z')
            offset = 'A';

        c -= offset;
        c = MOD(( c + distance ), 26);
        c += offset;

        assert(
            ( c >= 'a' && c <= 'z' ) ||
            ( c >= 'A' && c <= 'Z' )
         );
    }

    return c;
}

int main(int argc, char** argv) {

    if(argc < 2) {
        printf("Usage: %s <shift> [ <word> ... ]\n", argv[0]);
        return 1;
    }

    assert(argc >= 2);
    int shift = atoi(argv[1]);

    assert(shift > -26);
    assert(shift < 26);

    if(argc > 2) {
        for(int i=2; i<argc; i++) {
            for(int c=0; c<strlen(argv[i]); c++) {
                printf ("%c", do_shift(shift, argv[i][c]));
            }
            printf (" ");
        }
        printf("\n");
    } else {
        char buf[1];
        while(read(0, buf, sizeof(buf))>0) {
            printf ("%c", do_shift(shift, buf[0]));
        }
    }
}
